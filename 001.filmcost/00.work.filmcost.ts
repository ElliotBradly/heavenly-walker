import Beeing from "./00.core/beeing";
import * as B from "./00.core/constant/BASIC";

import * as HarkW from "./01.walker.unit/walker.hark";
import * as HikeW from "./01.walker.unit/walker.hike";

var EventEmitter = require("events").EventEmitter;

var Snap = require("snapsvg");

var sim = {
  wake: null,
  fate: null,
  bee: null,
  event: new EventEmitter(),
  render: null,
};

var frm0, frm1, frm2, frm3, frm4, frm5;
var orb0, orb1, orb2;
var rec0, rec1;
var orb = -1;

var filmFormat = ["16mm", "35mm", "super_8mm"];
var frameSize = ["2k", "4k"];

sim.wake = (bee: Beeing) => {
  sim.bee = bee;

  frm0 = document.getElementById("frm0");
  frm1 = document.getElementById("frm1");
  frm2 = document.getElementById("frm2");
  frm3 = document.getElementById("frm3");
  frm4 = document.getElementById("frm4");
  frm5 = document.getElementById("frm5");

  document.getElementById("orb0").style.height = "280px";
  document.getElementById("orb1").style.height = "280px";
  document.getElementById("orb2").style.height = "280px";

  orb0 = Snap("#orb0");
  orb1 = Snap("#orb1");
  orb2 = Snap("#orb2");

  rec0 = Snap("#rec0");
  rec1 = Snap("#rec1");

  var orbList = [orb0, orb1, orb2];
  var orbFormList = [frm0, frm1, frm2];

  var recList = [rec0, rec1];
  var recFormList = [frm4, frm5];

  filmFormat.forEach((a, b) => {
    var file = "./svg/" + a + "-default.svg";
    var orb = orbList[b];
    Snap.load(file, (svg) => {
      orb.append(svg);
      orb.transform(new Snap.matrix().scale(1.65).translate(80, 80));
    });
  });

  frameSize.forEach((a, b) => {
    var file = "./svg/" + a + "-default.svg";
    var rec = recList[b];
    Snap.load(file, (svg) => {
      rec.append(svg);
      rec.transform(new Snap.matrix().scale(1.95).translate(80, 50));
    });
  });

  orbFormList.forEach((a, b) => {
    a.addEventListener("mousedown", () => {
      alert(b);
    });

    a.addEventListener("mouseover", () => {
      // orbAction(b, orbList, "hover");
    });

    a.addEventListener("mouseout", () => {
      //orbAction(b, orbList, "default");
    });
  });

  recFormList.forEach((a, b) => {
    a.addEventListener("mousedown", () => {
      alert(b);
    });

    a.addEventListener("mouseover", () => {
      //orbAction(b, recList, "hover");
    });

    a.addEventListener("mouseout", () => {
      //orbAction(b, recList, "default");
    });
  });
};

var orbAction = (val, lst, act) => {
  var a = filmFormat[val];

  var file = "./svg/" + a + "-" + act + ".svg";
  var orb = lst[val];
  Snap.load(file, (svg) => {
    orb.append(svg);
    orb.transform(new Snap.matrix().scale(1.5).translate(25, 25));
  });

  lst[val] = orb;
};

var renderOrb = () => {};

var mousedown = (val) => {
  alert(val);
};

module.exports = sim;

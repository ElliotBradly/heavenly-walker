import TitleUnit from "./00.core/title/title.unit";
import Model from "./00.core/interface/model.interface";

import Title from "./00.core/title/fce/title.interface";
import { TitleModel } from "./00.core/title/title.model";

import * as reduceFromTitle from "./00.core/title/title.reduce";

import WalkerUnit from "./01.walker.unit/walker.unit";


import Walker from "./01.walker.unit/fce/walker.interface";
import { WalkerModel } from "./01.walker.unit/walker.model";


export const list: Array<any> = [TitleUnit,WalkerUnit];

import * as reduceFromWalker from "./01.walker.unit/walker.reduce";


export const reducer: any = {
 title: reduceFromTitle.reducer,
 walker : reduceFromWalker.reducer, 

};

export default class UnitData implements Model {
 auto: number = 0;

 rootData: string = "../data";
 localData: string = "./data";

 title: Title = new TitleModel();
 walker : Walker = new WalkerModel();

}

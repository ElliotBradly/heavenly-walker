import { Action } from "../00.core/interface/action.interface";
import  WalkerBit  from "./fce/walker.bit";

export const WALKER_OPEN = "[Walker Action] Waking Walker";
export class Walker implements Action {
 readonly type = WALKER_OPEN;
 constructor(public bale: WalkerBit) {}
}

export type Actions = Walker;

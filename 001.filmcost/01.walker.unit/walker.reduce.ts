import * as clone from "clone-deep";
import * as Act from "./walker.action";
import { WalkerModel } from "./walker.model";
import * as Buzz from "./walker.buzzer";
import State from "../00.core/state";

export function reducer(model: WalkerModel = new WalkerModel(), act: Act.Actions,  state?: State ) {
 switch (act.type) {
 case Act.WALKER_OPEN:
 return Buzz.writeWalker(clone(model), act.bale, state);

 default:
 return model;
 }
}

var trace = (msg) => console.log(msg);

const remote = require("electron").remote;

global.E = require("../dist/001.filmcost/00.core/constant/EVENT");
global.SIM = require("../dist/001.filmcost/00.core/0pen");

var run = () => {
  global.BEEING = require("../dist/001.filmcost/00.work.filmcost");
  BEEING.wake(SIM.bee);
};

SIM.event.on(E.COMPLETE, run);
SIM.wake();

import { Action } from "../00.core/interface/action.interface";
import  OpenBit  from "./fce/open.bit";

export const OPEN_OPEN = "[Open action] Waking Open";
export class Open implements Action {
 readonly type = OPEN_OPEN;
 constructor(public bale: OpenBit) {}
}

export type Actions = Open;

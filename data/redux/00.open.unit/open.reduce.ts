import * as clone from "clone-deep";
import * as Act from "./open.action";
import { OpenModel } from "./open.model";
import * as Buzz from "./open.buzzer";
import State from "../00.core/state";

export function reducer(model: OpenModel = new OpenModel(), act: Act.Actions,  state?: State ) {
 switch (act.type) {
 case Act.OPEN_OPEN:
 return Buzz.writeOpen(clone(model), act.bale, state);

 default:
 return model;
 }
}
